#!/bin/env python3
# GPE - GPG Python Encrypter 

import re, os, subprocess
import PySimpleGUI as sg, more_itertools as mi
import GPE

print("GPE started...")

## Loading available pub GPG keys from keyring
pub_keys = os.popen("gpg -k").readlines()

## Extracting emails and UIDs strings from output "gpg -k"

pub_keys = [ key.strip() for key in pub_keys if ("sub" or "pub" not in key) ]
easy_keys, expired_keys = mi.partition(lambda x: "expired" in x, pub_keys)
list(easy_keys).pop(0)

## Clean strings
neat_keys = []
for k in easy_keys:
    k = re.sub(r'^.*? <', '', k)
    k = re.sub('>', '', k)
    neat_keys.append(k)

for k in expired_keys:
    k = re.sub(r'^.*? <', '', k)
    k = re.sub('>', '', k)
    neat_keys.append(k + " - Expired key")

UIDs = []
emails = []

## Creates list with emails and UIDs from available  GPG keys
for e in neat_keys:
    if "@" in e:
        emails.append(e)
    else:
        UIDs.append(e)
emails_uid = list(zip(emails, UIDs))


## Color theme
sg.theme('Material1')

## Gui layout -- Elements of the interface
layout = [
    [sg.T("File to encrypt:")],
    [sg.In(key='file')],
    [sg.FileBrowse(target='file', enable_events=True, key='file')],
    [sg.T("Recipients list:")],
    [sg.Combo(emails, enable_events=True, key='rec')],
    [sg.Button('Add key'), sg.Button('Remove key'), sg.Button('Check List')],
    [sg.Button('Encrypt')],
    [sg.T("Output: ")],
    [sg.Multiline(size=(50,8), key='multiline')],
    [sg.T("GPE - GPG Python Encrypter GUI. 2022.")],
]

## Events loop - Starting GUI
window = sg.Window('GPE', layout)
gpe = GPE.GPE(window)
while True:
    event, values = window.read()
    if values is not None and 'file' in values:
        file = values['file']
    else:
        file = ''
    if values is not None and 'rec' in values:
        recipients = values['rec']
    else:
        recipients = []

## Prints the selected file
    if event == "file":
        print("File selected")
        window['multiline'].print("File selected")
    elif event == "Add key":
        gpe.add(recipients)
    elif event == "Remove key":
        gpe.remove(recipients)
    elif event == "Check List":
        gpe.checklist(recipients,file)
    elif event == 'Encrypt':
        gpe.encrypt(file)
    elif event == sg.WIN_CLOSED:
        break
