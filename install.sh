#!/usr/bin/bash
## GPE installer

## Is not necessary to install GPE, you can run it using python like ("./main.py" or "python main.py")
## The installer is just to have an icon launcher in your Desktop Environment

USAGE="Usage: install.sh [install|remove]"

if [ "$1" == "remove" ]  
then
  sudo rm -r /opt/gpe
  sudo rm /usr/share/icons/python_gpe.png
  sudo rm /usr/share/applications/gpe.desktop
  echo GPE removed.

elif [ "$1" == "install" ]
then
  echo "Installer started" 
  ## Create a gpe folder
  folder=$(pwd)
  echo "$folder"
  sudo mkdir /opt/gpe
  sudo cp -r $folder /opt
  echo "gpe folder created in /opt/"

  ## Copy icon and .desktop launcher
  sudo cp /opt/gpe/launcher/python_gpe.png /usr/share/icons/python_gpe.png
  echo "python_gpe.png copied in /usr/share/icons/python_gpe.png"
  sudo cp /opt/gpe/launcher/gpe.desktop /usr/share/applications/gpe.desktop
  sudo chmod +x /usr/share/applications/gpe.desktop
  echo gpe.desktop launcher copied in /usr/share/applications/gpe.desktop
else
  echo $USAGE
fi
