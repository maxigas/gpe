## Functions for GPE
import subprocess
import PySimpleGUI as sg

class GPE:
    def __init__(self, window):
        self.r = []
        self.window = window

    def add(self, recipients):
        """ Add recepient to selected_recepients list """
        if "@" not in recipients:
            print("You didn't select any recipients.")
            self.window['multiline'].print("You didn't select any recipients.")
            pass
        else:
            self.r.append(recipients)
            print("You added " +str(recipients)+ " to the list")
            self.window['multiline'].print("You added " +str(recipients)+ " to the list")

    def remove(self, recipients):
        """ Remove recepient from selected_recipients list """
        if len(self.r) == 0:
            print("Recipients list is empty.")
            self.window['multiline'].print("Recipients list is empty.")
            pass
        else:
            self.r.remove(recipients)
            print("You removed " +str(recipients)+ " from the list")
            self.window['multiline'].print("You removed " +str(recipients)+ " from the list")

    def checklist(self, recipients, file):
        """ Prints out who is in selected_recipients """
        if recipients == '' and file == '':
            print("No file and recipients selected.")
            self.window['multiline'].print("No file and recipients selected.")
            sg.popup("No file and receipients selected.")
        elif recipients == '':
            print("Selected file is " +file+ " and no recipients selected.")
            self.window['multiline'].print("Selected file is " +file+ " and no recipients selected.")
            sg.popup("Selected file is " +file+ " and no recipients selected.")
        elif file == '':
            print("No file selected and recipients are: " +str(self.r)+ ".")
            self.window['multiline'].print("No file selected and recipients are: " +str(self.r)+ ".")
            sg.popup("No file selected and recipients are: " +str(self.r)+ ".")
        else:
            print("Selected file is {} and recipient's UID: {}".format(file, self.r))
            self.window['multiline'].print("Selected file is '{}' and the recipients are: {}".format(file, self.r))
            sg.popup("Selected file is '{}' and the recipients are: {}".format(file, self.r))

    def encrypt(self, file):
        """ Encrypts file using gpg "gpg -r recepient@org filetoencrypt" """
        recepients_string = ""
        for r in self.r:
            recepients_string += str("-r "+r+" ")
#        command = "--always-trust --batch --yes " +recepients_string+ " -e " +file
        command = "--batch --yes " +recepients_string+ " -e " +file
        command_words = command.split()
        list_command_arguments = []
        for w in command_words:
            list_command_arguments.append(w)
        completed_process = subprocess.run(['gpg'] + list_command_arguments, capture_output=True)
        error_code = completed_process.returncode
        if file == '':
            print("No file selected to encrypt.")
            self.window['multiline'].print("No file selected to encrypt.")
            sg.popup("No file selected to encrypt.")
        elif recepients_string == "":
            print("No recipients selected.")
            self.window['multiline'].print("No recipients selected.")
            sg.popup("No recipients selected.")

        ## Ask confirmation to encrypt to unverified keys
        elif "no assurance this key belongs" in str(completed_process.stderr):
            print("Some of the selected GPG keys are unverified.")
            self.window['multiline'].print("Some of the selected GPG keys are unverified.")
            answer = sg.popup_yes_no('One or more GPG keys are unverified.\nDo you really want to encrypt to unverified keys?', title="Unverified keys")
            if answer == "Yes":
                list_command_arguments.insert(0, "--always-trust ")
                completed_process = subprocess.run(['gpg'] + list_command_arguments, capture_output=True)
                print("Encryption successful!")
                self.window['multiline'].print("Encryption successful!")
                sg.popup("Encryption successful!\n\n You're file is saved in " + str(file) + ".gpg")
            elif answer == "No":
               print("File not encrypted.")
               self.window['multiline'].print("File not encrypted.")
               sg.popup("File not encrypted.")

        elif error_code == 0:
            print("Encryption successful!")
            self.window['multiline'].print("Encryption successful!")
            sg.popup("Encryption successful!\n\n You're file is saved in " + str(file) + ".gpg", title="Encryption done.")
        elif error_code < 0:
            print("The process died before it completed. Please try again.")
            self.window['multiline'].print("The process died before it was completed. Please try again.")
            sg.popup("The process died before it was completed. Please try a gain.")
        elif error_code == 2:
            self.window['multiline'].print("Encryption failed with exit code " +str(error_code)+ ".\n" + str(completed_process.stderr))
            sg.popup("Encryption failed with exit code: " + str(error_code) + ".\n\nAre the selected keys expired?\n Check the logs for more info.")
        else:
            print("Encryption failed with exit code " +str(error_code)+ ".\n" +str(completed_process.stderr))
            self.window['multiline'].print("Encryption failed with exit code " +str(error_code)+ ".\n" + str(completed_process.stderr))
            sg.popup("Encryption failed with exit code: " + str(error_code))
