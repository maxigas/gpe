# GPE

## GUI File encrypter with GPG
GPE is a GUI for encrypting files using GPG written in python using the library "pysimplegui".
It uses gpg commands to encrypt files with the public keys that are added to your keyring. 

## Dependencies:
- python3
- pysimplegui (pip to install it)
- gnupg
- tk

## How to run it:
- Clone the repository 'git clone https://git.puscii.nl/totalreset/gpe'
- Install pysimplegui 'pip install pysimplegui'
- Make it executable 'chmod +x main.py'
- Run it (ex './main.py')

## How it works:
- Click "Browse" to select a file 
- Add recipients with the dropdown and the button "Add key"
- Check the list of selected receipients (optional)
- Click "Encrypt" to encrypt the file

## Installer
There is no need of installing GPE, the installer just copies the 'gpe' folder in /opt/ and creates a .desktop icon launcher. If you don't care about the launcher don't run the installer.
In case you want just simply do "bash install.sh install"
To remove/uninstall gpe "bash install.sh remove"

## What to do:
- Clean up code
- Improve text in GUI
- Function to check if GPG key is verified?
- Import/check GPG keys function? 

## Screenshot
![](/screenshots/GUI.jpeg)
